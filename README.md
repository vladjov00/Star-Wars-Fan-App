## Star Wars Fan App

A web application designed to search for information related to the beloved Star Wars universe. <br>
View detailed information about characters, planets, vehicles, starships, species and films.

The project was made using [nodejs](https://nodejs.org/en) and [Angular](https://angular.io/). <br>
All Star Wars data is fetched from the [SWAPI](https://swapi.dev/).

---

## Installation guide

1. Clone this repository
2. Change directory to star-wars-fan-app
3. Run ```./install_modules``` script
4. Run ```node server.js``` inside _server_ directory
5. Run ```ng serve``` inside _client_ directory

---

## Usage

After installing, open ```http://localhost:4200```. On the login screen, you can choose to register with a new account, or log in using a default account:
```
username: johndoe
password: abcde
```
When the __Home page__ loads, GET requests will be sent to _https://swapi.dev/_ to fetch all data. Until all data is fetched, the __Search Bar__ won't be visible.<br>
After all data is loaded, you are free to use the __Search Bar__, as well as browse through resources on their respective tabs in the __Navigation Menu__.

---

## About

Author: Vladimir Jovanović <br>
Date: October 2023