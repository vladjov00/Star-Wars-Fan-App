const express = require('express');
const controller = require('./speciesController');

const router = express.Router();

router.get('/', controller.getSpeciesByPage);
router.get('/all', controller.getAllSpeciesNames);
router.get('/id/:id', controller.getSpeciesById);

module.exports = router;