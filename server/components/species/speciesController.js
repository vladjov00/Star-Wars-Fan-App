const swapi = require('swapi-node');

let speciesArray = [];
let getRequestAlreadySent = false;

let pagesArray = [];

module.exports.getAllSpeciesNames = async (req, res, next) => {
    if(getRequestAlreadySent) {
        res.status(200).json(speciesArray);
        return;
    }
    
    let pageNumber = 1;
    let hasMorePages = true;
    let species = [];

    try {
        while(hasMorePages) {
            const result = await swapi.get(`https://swapi.dev/api/species/?page=${pageNumber}`).then((page) => {
                pagesArray[pageNumber] = page;
                const array = page.results;
                const spcs = [];
                array.forEach(spc => {
                    spcs.push({ name: spc.name, url: spc.url });
                });
                return spcs;
            }).catch((err) => {
                hasMorePages = false;
            });

            species = species.concat(result);
            pageNumber = pageNumber + 1;
        }
        getRequestAlreadySent = true;
        speciesArray = [...species];
        res.status(200).json(species);
    } catch(err) {
        next(err);
    }
}

module.exports.getSpeciesByPage = async (req, res, next) => {
    const page = req.query.page;
    if(pagesArray[page] !== undefined) {
        res.status(200).json(pagesArray[page]);
        return;
    }

    try {
        const speciesPage = await swapi.get(`https://swapi.dev/api/species/?page=${page}`);
        pagesArray[page] = speciesPage;
        res.status(200).json(speciesPage);
    } catch (err) {
        next(err);
    }
};

module.exports.getSpeciesById = async (req, res, next) => {
    try {
        const id = req.params.id;

        const species = await swapi.species({id: id});

        if (species === null) {
            const error = new Error(`Species with id ${id} is not found`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(species);

    } catch (err) {
        next(err);
    }
};