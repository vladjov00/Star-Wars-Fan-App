const swapi = require('swapi-node');

let planetsArray = [];
let getRequestAlreadySent = false;

let pagesArray = [];

module.exports.getAllPlanetsNames = async (req, res, next) => {
    if(getRequestAlreadySent) {
        res.status(200).json(planetsArray);
        return;
    }
    
    let pageNumber = 1;
    let hasMorePages = true;
    let planets = [];

    try {
        while(hasMorePages) {
            const result = await swapi.get(`https://swapi.dev/api/planets/?page=${pageNumber}`).then((page) => {
                pagesArray[pageNumber] = page;
                const array = page.results;
                const plnts = [];
                array.forEach(planet => {
                    plnts.push({ name: planet.name, url: planet.url });
                });
                return plnts;
            }).catch((err) => {
                hasMorePages = false;
            });

            planets = planets.concat(result);
            pageNumber = pageNumber + 1;
        }
        getRequestAlreadySent = true;
        planetsArray = [...planets];
        res.status(200).json(planets);
    } catch(err) {
        next(err);
    }
}

module.exports.getPlanetsByPage = async (req, res, next) => {
    const page = req.query.page;
    if(pagesArray[page] !== undefined) {
        res.status(200).json(pagesArray[page]);
        return;
    }

    try {
        const planetsPage = await swapi.get(`https://swapi.dev/api/planets/?page=${page}`);
        pagesArray[page] = planetsPage;
        res.status(200).json(planetsPage);
    } catch (err) {
        next(err);
    }
};

module.exports.getPlanetById = async (req, res, next) => {
    try {
        const id = req.params.id;

        const planet = await swapi.planets({id: id});

        if (planet === null) {
            const error = new Error(`Planet with id ${id} is not found`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(planet);

    } catch (err) {
        next(err);
    }
};