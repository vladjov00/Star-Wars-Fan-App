const express = require('express');
const controller = require('./planetsController');

const router = express.Router();

router.get('/', controller.getPlanetsByPage);
router.get('/all', controller.getAllPlanetsNames);
router.get('/id/:id', controller.getPlanetById);

module.exports = router;