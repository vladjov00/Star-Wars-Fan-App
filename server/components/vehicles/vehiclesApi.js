const express = require('express');
const controller = require('./vehiclesController');

const router = express.Router();

router.get('/', controller.getVehiclesByPage);
router.get('/all', controller.getAllVehiclesNames);
router.get('/id/:id', controller.getVehicleById);

module.exports = router;