const swapi = require('swapi-node');

let vehiclesArray = [];
let getRequestAlreadySent = false;

let pagesArray = [];

module.exports.getAllVehiclesNames = async (req, res, next) => {
    if(getRequestAlreadySent) {
        res.status(200).json(vehiclesArray);
        return;
    }
    
    let pageNumber = 1;
    let hasMorePages = true;
    let vehicles = [];

    try {
        while(hasMorePages) {
            const result = await swapi.get(`https://swapi.dev/api/vehicles/?page=${pageNumber}`).then((page) => {
                pagesArray[pageNumber] = page;
                const array = page.results;
                const vhcls = [];
                array.forEach(vhc => {
                    vhcls.push({ name: vhc.name, url: vhc.url });
                });
                return vhcls;
            }).catch((err) => {
                hasMorePages = false;
            });

            vehicles = vehicles.concat(result);
            pageNumber = pageNumber + 1;
        }
        getRequestAlreadySent = true;
        vehiclesArray = [...vehicles];
        res.status(200).json(vehicles);
    } catch(err) {
        next(err);
    }
}
 
module.exports.getVehiclesByPage = async (req, res, next) => {
    const page = req.query.page;
    if(pagesArray[page] !== undefined) {
        res.status(200).json(pagesArray[page]);
        return;
    }

    try {
        const vehiclesPage = await swapi.get(`https://swapi.dev/api/vehicles/?page=${page}`);
        pagesArray[page] = vehiclesPage;
        res.status(200).json(vehiclesPage);
    } catch (err) {
        next(err);
    }
};

module.exports.getVehicleById = async (req, res, next) => {
    try {
        const id = req.params.id;

        const vehicle = await swapi.vehicles({id: id});

        if (vehicle === null) {
            const error = new Error(`Vehicle with id ${id} is not found`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(vehicle);

    } catch (err) {
        next(err);
    }
};