const swapi = require('swapi-node');

let starshipsArray = [];
let getRequestAlreadySent = false;

let pagesArray = [];

module.exports.getAllStarshipsNames = async (req, res, next) => {
    if(getRequestAlreadySent) {
        res.status(200).json(starshipsArray);
        return;
    }
    
    let pageNumber = 1;
    let hasMorePages = true;
    let starships = [];

    try {
        while(hasMorePages) {
            const result = await swapi.get(`https://swapi.dev/api/starships/?page=${pageNumber}`).then((page) => {
                pagesArray[pageNumber] = page;
                const array = page.results;
                const strshps = [];
                array.forEach(str => {
                    strshps.push({ name: str.name, url: str.url });
                });
                return strshps;
            }).catch((err) => {
                hasMorePages = false;
            });

            starships = starships.concat(result);
            pageNumber = pageNumber + 1;
        }
        getRequestAlreadySent = true;
        starshipsArray = [...starships];
        res.status(200).json(starships);
    } catch(err) {
        next(err);
    }
}
 

module.exports.getStarshipsByPage = async (req, res, next) => {
    const page = req.query.page;
    if(pagesArray[page] !== undefined) {
        res.status(200).json(pagesArray[page]);
        return;
    }

    try {
        const starshipsPage = await swapi.get(`https://swapi.dev/api/starships/?page=${page}`);
        pagesArray[page] = starshipsPage;
        res.status(200).json(starshipsPage);
    } catch (err) {
        next(err);
    }
};

module.exports.getStarshipById = async (req, res, next) => {
    try {
        const id = req.params.id;

        const starship = await swapi.starships({id: id});

        if (starship === null) {
            const error = new Error(`Starship with id ${id} is not found`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(starship);

    } catch (err) {
        next(err);
    }
};