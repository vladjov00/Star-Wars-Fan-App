const express = require('express');
const controller = require('./starshipsController');

const router = express.Router();

router.get('/', controller.getStarshipsByPage);
router.get('/all', controller.getAllStarshipsNames);
router.get('/id/:id', controller.getStarshipById);

module.exports = router;