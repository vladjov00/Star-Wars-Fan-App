const User = require('./usersModel');

module.exports.canAuthenticate = async (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;

  try {
    if (!username || !password) {
      const error = new Error('Please provide both username and password');
      error.status = 400;
      throw error;
    }

    const user = User.getUserByUsername(username);
    if (!user) {
      const error = new Error(`User with username ${username} does not exist!`);
      error.status = 404;
      throw error;
    }

    if (user.password !== password) {
      const error = new Error(`Wrong password for username ${username}!`);
      error.status = 401;
      throw error;
    }

    req.userId = user.id;
    req.username = user.username;

    next();
  } catch (err) {
    next(err);
  }
};