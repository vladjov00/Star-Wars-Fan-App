const express = require('express');
const controller = require('./usersController');
const authentication = require('./authentication');

const router = express.Router();

router.get('/', controller.getAllUsers);
router.post('/register', controller.addNewUser);
router.post('/login', authentication.canAuthenticate, controller.loginUser);

module.exports = router;