const users = require('./usersModel');

module.exports.addNewUser = (req, res) => {
  const { username, password, name } = req.body;

  if (
    !username ||
    !password ||
    !name
  ) {
    res.status(400).json();
  } else {
    const jwt = users.addNewUser(username, password, name);
    return res.status(201).json({
      token: jwt,
    });
  }
};

module.exports.loginUser = (req, res) => {
  const { username, password } = req.body;
  
  if (
    !username ||
    !password 
  ) {
    res.status(400).json();
  } else {
    try {
      const jwt = users.getUserJWTByUsername(username);
      return res.status(201).json({
        token: jwt,
      });
    } catch (err) {
      next(err);
    }
  }
};

module.exports.getAllUsers = (req, res) => {
  const userData = users.getAllUsers();
  res.status(200).json(userData);
}