const uuid = require('uuid');
const jwt = require('./jwt');

const users = [
  {
    id: '996bcb75-f0cc-4cf0-afcd-5a491c13172a',
    username: 'johndoe',
    password: 'abcde',
    name: 'John Doe',
  }
];

const getAllUsers = () => {
  return users;
};

const getUserByUsername = (username) => {
  const findUsers = users.filter((user) => user.username == username);
  return findUsers.length > 0 ? findUsers[0] : null;
};

/**
 * Kreira JSON Web Token sa podacima o korisniku.
 * @param {string} username Korisnicko ime.
 * @returns {Promise<string>} JWT sa podacima o korisniku sa datim korisnickim imenom.
 */
const getUserJWTByUsername = (username) => {
  const user = getUserByUsername(username);
  if (!user) {
    throw new Error(`User with username ${username} does not exist!`);
  }
  return jwt.generateJWT({
    id: user.id,
    username: user.username,
    name: user.name
  });
}

const addNewUser = (username, password, name) => {
  const newUser = {
    id: uuid.v4(),
    username,
    password,
    name
  };

  const user = getUserByUsername(username);

  if (user == null) {
    users.push(newUser);
  }

  return getUserJWTByUsername(username);
};

module.exports = {
  getAllUsers,
  getUserByUsername,
  addNewUser,
  getUserJWTByUsername
};
