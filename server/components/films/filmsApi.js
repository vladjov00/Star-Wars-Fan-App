const express = require('express');
const controller = require('./filmsController');

const router = express.Router();

router.get('/', controller.getAllFilmsNames);
router.get('/id/:id', controller.getFilmById);

module.exports = router;