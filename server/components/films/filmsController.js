const swapi = require('swapi-node');

let filmsArray = [];
let getRequestAlreadySent = false;

module.exports.getAllFilmsNames = async (req, res, next) => {
    if(getRequestAlreadySent) {
        res.status(200).json(filmsArray);
        return;
    }
    try {
        const films = await swapi.get('https://swapi.dev/api/films').then((page) => {
            return page.results;
            /* const array = page.results;
            const flms = [];
            array.forEach(flm => {
                flms.push({ name: flm.title, url: flm.url });
            });
            return flms; */
        });
        filmsArray = [...films];
        getRequestAlreadySent = true;
        res.status(200).json(films);
    } catch (err) {
        next(err);
    }
};

module.exports.getFilmById = async (req, res, next) => {
    try {
        const id = req.params.id;

        const film = await swapi.films({id: id});

        if (film === null) {
            const error = new Error(`Film with id ${id} is not found`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(film);

    } catch (err) {
        next(err);
    }
};