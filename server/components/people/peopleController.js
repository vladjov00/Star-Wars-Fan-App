const swapi = require('swapi-node');

let peopleArray = [];
let getRequestAlreadySent = false;

let pagesArray = [];

module.exports.getPeopleByPage = async (req, res, next) => {
    const page = req.query.page;
    if(pagesArray[page] !== undefined) {
        res.status(200).json(pagesArray[page]);
        return;
    }

    try {
        const peoplePage = await swapi.get(`https://swapi.dev/api/people/?page=${page}`);
        pagesArray[page] = peoplePage;
        res.status(200).json(peoplePage);
    } catch (err) {
        next(err);
    }
};

module.exports.getPersonById = async (req, res, next) => {
    try {
        const id = req.params.id;

        const person = await swapi.people({id: id});

        if (person === null) {
            const error = new Error(`Person with id ${id} is not found`);
            error.status = 404;
            throw error;
        }
        res.status(200).json(person);
    } catch (err) {
        next(err);
    }
};

module.exports.getAllPeopleNames = async (req, res, next) => {
    if(getRequestAlreadySent) {
        res.status(200).json(peopleArray);
        return;
    }
    
    let pageNumber = 1;
    let hasMorePages = true;
    let people = [];

    try {
        while(hasMorePages) {
            const result = await swapi.get(`https://swapi.dev/api/people/?page=${pageNumber}`).then((page) => {
                pagesArray[pageNumber] = page;
                const array = page.results;
                const ppl = [];
                array.forEach(person => {
                    ppl.push({ name: person.name, url: person.url });
                });
                return ppl;
            }).catch((err) => {
                hasMorePages = false;
            });

            people = people.concat(result);
            pageNumber = pageNumber + 1;
        }
        getRequestAlreadySent = true;
        peopleArray = [...people];
        res.status(200).json(people);
    } catch(err) {
        next(err);
    }
}