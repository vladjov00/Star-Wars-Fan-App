const express = require('express');
const controller = require('./peopleController');

const router = express.Router();

router.get('/', controller.getPeopleByPage);
router.get('/all', controller.getAllPeopleNames)
router.get('/id/:id', controller.getPersonById);

module.exports = router;