const express = require('express');
const cors = require('cors')

const usersAPI = require('./components/users/usersApi');
const peopleAPI = require('./components/people/peopleApi');
const filmsAPI = require('./components/films/filmsApi');
const planetsAPI = require('./components/planets/planetsApi');
const starshipsAPI = require('./components/starships/starshipsApi');
const vehiclesAPI = require('./components/vehicles/vehiclesApi');
const speciesAPI = require('./components/species/speciesApi');

const app = express();

app.use(
    express.urlencoded({
        extended: false
    })
);
app.use(express.json());
app.use(cors());

app.use('/api/users', usersAPI);
app.use('/api/people', peopleAPI);
app.use('/api/films', filmsAPI);
app.use('/api/planets', planetsAPI);
app.use('/api/starships', starshipsAPI);
app.use('/api/vehicles', vehiclesAPI);
app.use('/api/species', speciesAPI);

app.use(function (req, res, next) {
    const error = new Error('Zahtev nije podrzan od servera');
    error.status = 405;
  
    next(error);
});

app.use(function (error, req, res, next) {
    const statusCode = error.status || 500;
    res.status(statusCode).json({
      message: error.message,
      status: statusCode,
      stack: error.stack,
    });
});

module.exports = app;