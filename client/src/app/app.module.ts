import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PeopleComponent } from './components/people/people.component';
import { PersonInfoComponent } from './components/people/person-info/person-info.component';
import { PersonItemComponent } from './components/people/person-item/person-item.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { VehicleItemComponent } from './components/vehicles/vehicle-item/vehicle-item.component';
import { VehicleInfoComponent } from './components/vehicles/vehicle-info/vehicle-info.component';
import { StarshipsComponent } from './components/starships/starships.component';
import { StarshipItemComponent } from './components/starships/starship-item/starship-item.component';
import { StarshipInfoComponent } from './components/starships/starship-info/starship-info.component';
import { SpeciesComponent } from './components/species/species.component';
import { SpeciesItemComponent } from './components/species/species-item/species-item.component';
import { SpeciesInfoComponent } from './components/species/species-info/species-info.component';
import { FilmsComponent } from './components/films/films.component';
import { FilmItemComponent } from './components/films/film-item/film-item.component';
import { FilmInfoComponent } from './components/films/film-info/film-info.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { PlanetItemComponent } from './components/planets/planet-item/planet-item.component';
import { PlanetInfoComponent } from './components/planets/planet-info/planet-info.component';
import { LoginPageComponent } from './components/user-info/login-page/login-page.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterPageComponent } from './components/user-info/register-page/register-page.component';
import { NavigationBarComponent } from './components/home-page/navigation-bar/navigation-bar.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LogoutComponent } from './components/user-info/logout/logout.component';
import { PeoplePaginationComponent } from './components/people/people-pagination/people-pagination.component';
import { VehiclesPaginationComponent } from './components/vehicles/vehicles-pagination/vehicles-pagination.component';
import { StarshipsPaginationComponent } from './components/starships/starships-pagination/starships-pagination.component';
import { SpeciesPaginationComponent } from './components/species/species-pagination/species-pagination.component';
import { PlanetsPaginationComponent } from './components/planets/planets-pagination/planets-pagination.component';
import { SearchComponent } from './components/search/search.component';
import { ClickOutsideDirective } from './directives/click-outside.directive';

@NgModule({
  declarations: [
    AppComponent,
    PeopleComponent,
    PersonInfoComponent,
    PersonItemComponent,
    VehiclesComponent,
    VehicleItemComponent,
    VehicleInfoComponent,
    StarshipsComponent,
    StarshipItemComponent,
    StarshipInfoComponent,
    SpeciesComponent,
    SpeciesItemComponent,
    SpeciesInfoComponent,
    FilmsComponent,
    FilmItemComponent,
    FilmInfoComponent,
    PlanetsComponent,
    PlanetItemComponent,
    PlanetInfoComponent,
    LoginPageComponent,
    RegisterPageComponent,
    HomePageComponent,
    NavigationBarComponent,
    LogoutComponent,
    PeoplePaginationComponent,
    VehiclesPaginationComponent,
    StarshipsPaginationComponent,
    SpeciesPaginationComponent,
    PlanetsPaginationComponent,
    SearchComponent,
    ClickOutsideDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
