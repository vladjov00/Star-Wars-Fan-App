import { Injectable } from '@angular/core';
import { IJwtTokenData } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private static readonly USER_TOKEN_ID: string = 'USER_JWT_TOKEN';

  constructor() { }

  public setToken(jwt: string): void {
    localStorage.setItem(JwtService.USER_TOKEN_ID, jwt);
  }

  public getToken(): string | null {
    const token: string | null = localStorage.getItem(JwtService.USER_TOKEN_ID);

    return token;
  }

  public removeToken(): void {
    localStorage.removeItem(JwtService.USER_TOKEN_ID);
  }

  public getDataFromToken(): IJwtTokenData | null {
    const token = this.getToken();
    if(!token) {
      return null;
    }

    const payloadString = token.split('.')[1];
    const userDataJson: string = atob(payloadString);

    const payload: IJwtTokenData = JSON.parse(userDataJson);

    return payload;
  }

}
