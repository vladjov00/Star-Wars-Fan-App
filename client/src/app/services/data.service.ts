import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Film } from '../models/film.model';
import { PeoplePagination, Person } from '../models/person.model';
import { Planet, PlanetsPagination } from '../models/planet.model';
import { Species, SpeciesPagination } from '../models/species.model';
import { Starship, StarshipsPagination } from '../models/starship.model';
import { Vehicle, VehiclesPagination } from '../models/vehicle.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private urls = {
    peopleURL: "http://localhost:3000/api/people/",
    filmsURL: "http://localhost:3000/api/films/",
    vehiclesURL: "http://localhost:3000/api/vehicles/",
    planetsURL: "http://localhost:3000/api/planets/",
    speciesURL: "http://localhost:3000/api/species/",
    starshipsURL: "http://localhost:3000/api/starships/"
  };

  private item: Person | Species | Planet | Starship | Vehicle | Film | null = null;

  constructor(private http: HttpClient) { 

  }

  public setItem(item: Person | Species | Planet | Starship | Vehicle | Film): void {
    this.item = item;
  }

  public getItem(): Person | Species | Planet | Starship | Vehicle | Film | null {
    return this.item;
  }

  public getAllPeopleNames(): Observable<{ name: string, url: string }[]> {
    return this.http.get<{ name: string, url: string }[]>(this.urls.peopleURL + 'all');
  }

  public getPeopleByPage(page: number = 1): Observable<PeoplePagination> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString());
    return this.http.get<PeoplePagination>(this.urls.peopleURL, { params: queryParams });
  }

  public getPersonById(personId: number): Observable<Person> {
    return this.http.get<Person>(this.urls.peopleURL + 'id/' + personId);
  }

  public getAllFilmsNames(): Observable<Film[]> {
    return this.http.get<Film[]>(this.urls.filmsURL);
  }

  public getFilmById(filmId: number): Observable<Film> {
    return this.http.get<Film>(this.urls.filmsURL + 'id/' + filmId);
  }

  public getAllVehiclesNames(): Observable<{ name: string, url: string }[]> {
    return this.http.get<{ name: string, url: string }[]>(this.urls.vehiclesURL + 'all');
  }

  public getVehiclesByPage(page: number = 1): Observable<VehiclesPagination> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString());
    return this.http.get<VehiclesPagination>(this.urls.vehiclesURL, { params: queryParams });
  }

  public getVehicleById(vehicleId: number): Observable<Vehicle> {
    return this.http.get<Vehicle>(this.urls.vehiclesURL + 'id/' + vehicleId);
  }

  public getAllStarshipsNames(): Observable<{ name: string, url: string }[]> {
    return this.http.get<{ name: string, url: string }[]>(this.urls.starshipsURL + 'all');
  }

  public getStarshipsByPage(page: number = 1): Observable<StarshipsPagination> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString());
    return this.http.get<StarshipsPagination>(this.urls.starshipsURL, { params: queryParams });
  }

  public getStarshipById(starshipId: number): Observable<Starship> {
    return this.http.get<Starship>(this.urls.starshipsURL + 'id/' + starshipId);
  }

  public getAllSpeciesNames(): Observable<{ name: string, url: string }[]> {
    return this.http.get<{ name: string, url: string }[]>(this.urls.speciesURL + 'all');
  }

  public getSpeciesByPage(page: number = 1): Observable<SpeciesPagination> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString());
    return this.http.get<SpeciesPagination>(this.urls.speciesURL, { params: queryParams });
  }

  public getSpeciesById(speciesId: number): Observable<Species> {
    return this.http.get<Species>(this.urls.speciesURL + 'id/' + speciesId);
  }

  public getAllPlanetsNames(): Observable<{ name: string, url: string }[]> {
    return this.http.get<{ name: string, url: string }[]>(this.urls.planetsURL + 'all');
  }

  public getPlanetsByPage(page: number = 1): Observable<PlanetsPagination> {
    const queryParams: HttpParams = new HttpParams().append("page", page.toString());
    return this.http.get<PlanetsPagination>(this.urls.planetsURL, { params: queryParams });
  }

  public getPlanetById(planetId: number): Observable<Planet> {
    return this.http.get<Planet>(this.urls.planetsURL + 'id/' + planetId);
  }
}
