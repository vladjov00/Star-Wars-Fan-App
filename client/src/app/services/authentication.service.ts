import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, Observable, Subject, tap } from 'rxjs';
import { IJwtTokenData, User } from '../models/user.model';
import { JwtService } from './jwt.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private readonly urls = {
    registerURL: "http://localhost:3000/api/users/register",
    loginURL: "http://localhost:3000/api/users/login"
  }

  private readonly userSubject: Subject<User | null> = new Subject<User | null>();
  public readonly user: Observable<User | null> = this.userSubject.asObservable();

  constructor (
    private http: HttpClient,
    private jwt: JwtService
  ) { }

  public sendUserDataIfExists(): User | null {
    const payload: IJwtTokenData | null = this.jwt.getDataFromToken();
    if(!payload) {
      return null;
    }
    const newUser: User = new User(payload.id, payload.username, payload.name);
    this.userSubject.next(newUser);
    return newUser;
  }

  public loginUser(username: string, password: string): Observable<User | null | string> {
    const body = {
      username,
      password,
    };

    const obs = this.http.post<{token: string}>(this.urls.loginURL, body);

    return obs.pipe(
      tap((response: {token: string}) => this.jwt.setToken(response.token)),
      map((response: {token: string}) => this.sendUserDataIfExists()),
      catchError(this.returnErrorAsString())
    );
  }
  
  private returnErrorAsString() {
    return (error: any): string => {
      return `Login failed: ${error.error.message}`;
    };
  }

  public registerUser(name: string, username: string, password: string): Observable<User | null> {
    const body = {
      username,
      password,
      name
    };

    const obs = this.http.post<{token: string}>(this.urls.registerURL, body);
    return obs.pipe(
      tap((response: {token: string}) => { this.jwt.setToken(response.token) }),
      map((response: {token: string}) => { return this.sendUserDataIfExists(); })
    );
  }

  public logoutUser(): void {
    this.jwt.removeToken();
    this.userSubject.next(null);
  }
}
