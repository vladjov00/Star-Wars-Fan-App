import { Injectable } from '@angular/core';
import FuzzySearch from 'fuzzy-search';
import { from, Observable, of, Subject } from 'rxjs';
import { Film } from '../models/film.model';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private peopleList: { name: string, id: string, url: string, type: string }[] = [];
  private planetsList: { name: string, id: string, url: string,  type: string }[] = [];
  private speciesList: { name: string, id: string, url: string,  type: string }[] = [];
  private vehiclesList: { name: string, id: string, url: string,  type: string }[] = [];
  private starshipsList: { name: string, id: string, url: string,  type: string }[] = [];
  private filmsList: { name: string, id: string, url: string, type: string }[] = [];

  public peopleLoaded: boolean = false;
  public planetsLoaded: boolean = false;
  public speciesLoaded: boolean = false;
  public filmsLoaded: boolean = false;
  public starshipsLoaded: boolean = false;
  public vehiclesLoaded: boolean = false;

  public loadedDataSubject = new Subject<string>();

  constructor() { }

  public search(term: string): Observable<{ name: string, id: string, url: string, type: string }[]> {
    if(!term.trim()) {
      return of([]);
    }
    const searcher = new FuzzySearch(
      this.getAllItemsList(),
      ['name'], 
      {
        caseSensitive: false,
      }
    );
    return of(searcher.search(term));
  }

  public searchResourceByUrl(term: string): Observable<{ name: string, id: string, url: string, type: string }[]> {
    const searcher = new FuzzySearch(
      this.getAllItemsList(),
      ['url']
    );
    return of(searcher.search(term));
  }

  public loadedData(type: string) {
    this.loadedDataSubject.next(type);
  }

  public setPeopleList(people: { name: string, url: string }[]) {
    this.peopleList = [];
    people.forEach(person => {
      if(!person) return;
      const split = person.url.split('/');
      const id = split[split.length - 2];
      this.peopleList.push({
        name: person.name,
        id: id,
        url: person.url,
        type: 'Person'
      });
    });
    this.peopleLoaded = true;
    this.loadedData('people');
    console.log(this.peopleList);
  }

  public getPeopleList(): { name: string, id: string, url: string, type: string }[] {
    return this.peopleList;
  }

  public setPlanetsList(planets: { name: string, url: string }[]) {
    this.planetsList = [];
    planets.forEach(planet => {
      if(!planet) return;
      const split = planet.url.split('/');
      const id = split[split.length - 2];
      this.planetsList.push({
        name: planet.name,
        id: id,
        url: planet.url,
        type: 'Planet'
      });
    });
    this.planetsLoaded = true;
    this.loadedData('planets');
    console.log(this.planetsList);
  }

  public getPlanetsList(): { name: string, id: string, url: string, type: string }[] {
    return this.planetsList;
  }

  public setStarshipsList(starships: { name: string, url: string }[]) {
    this.starshipsList = [];
    starships.forEach(starship => {
      if(!starship) return;
      const split = starship.url.split('/');
      const id = split[split.length - 2];
      this.starshipsList.push({
        name: starship.name,
        id: id,
        url: starship.url,
        type: 'Starship'
      });
    });
    this.starshipsLoaded = true;
    this.loadedData('starships');
    console.log(this.starshipsList);
  }

  public getStarshipsList():{ name: string, id: string, url: string, type: string }[] {
    return this.starshipsList;
  }

  public setSpeciesList(species: { name: string, url: string }[]) {
    this.speciesList = [];
    species.forEach(spc => {
      if(!spc) return;
      const split = spc.url.split('/');
      const id = split[split.length - 2];
      this.speciesList.push({
        name: spc.name,
        id: id,
        url: spc.url,
        type: 'Species'
      });
    });
    this.speciesLoaded = true;
    this.loadedData('species');
    console.log(this.speciesList);
  }

  public getSpeciesList(): { name: string, id: string, url: string, type: string }[] {
    return this.speciesList;
  }

  public setVehiclesList(vehicles: { name: string, url: string }[]) {
    this.vehiclesList = [];
    vehicles.forEach(vehicle => {
      if(!vehicle) return;
      const split = vehicle.url.split('/');
      const id = split[split.length - 2];
      this.vehiclesList.push({
        name: vehicle.name,
        id: id,
        url: vehicle.url,
        type: 'Vehicle'
      });
    });
    this.vehiclesLoaded = true;
    this.loadedData('vehicles');
    console.log(this.vehiclesList);
  }

  public getVehiclesList(): { name: string, id: string, url: string, type: string }[] {
    return this.vehiclesList;
  }

  public setFilmsList(films: Film[]) {
    this.filmsList = [];
    films.forEach(film => {
      const split = film.url.split('/');
      const id = split[split.length - 2];
      this.filmsList.push({
        name: film.title,
        id: id,
        url: film.url,
        type: 'Film'
      });
    });
    this.filmsLoaded = true;
    this.loadedData('films');
    console.log(this.filmsList);
  }

  public getFilmsList(): { name: string, id: string, url: string, type: string }[] {
    return this.filmsList;
  }

  public getAllItemsList(): { name: string, id: string, url: string, type: string }[] {
    return this.getPeopleList()
      .concat(this.getPlanetsList())
      .concat(this.getVehiclesList())
      .concat(this.getSpeciesList())
      .concat(this.getStarshipsList())
      .concat(this.getFilmsList());
  }
}
