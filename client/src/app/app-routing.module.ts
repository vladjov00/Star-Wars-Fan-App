import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FilmInfoComponent } from './components/films/film-info/film-info.component';
import { LoginPageComponent } from './components/user-info/login-page/login-page.component';
import { PersonInfoComponent } from './components/people/person-info/person-info.component';
import { PlanetInfoComponent } from './components/planets/planet-info/planet-info.component';
import { RegisterPageComponent } from './components/user-info/register-page/register-page.component';
import { SpeciesInfoComponent } from './components/species/species-info/species-info.component';
import { StarshipInfoComponent } from './components/starships/starship-info/starship-info.component';
import { VehicleInfoComponent } from './components/vehicles/vehicle-info/vehicle-info.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { LogoutComponent } from './components/user-info/logout/logout.component';
import { PeopleComponent } from './components/people/people.component';
import { FilmsComponent } from './components/films/films.component';
import { VehiclesComponent } from './components/vehicles/vehicles.component';
import { PlanetsComponent } from './components/planets/planets.component';
import { StarshipsComponent } from './components/starships/starships.component';
import { SpeciesComponent } from './components/species/species.component';

const routes: Routes = [
  { path: '', component: LoginPageComponent },
  { path: 'home', component: HomePageComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'register', component: RegisterPageComponent },
  { path: 'people', component: PeopleComponent },
  { path: 'people/info/:id', component: PersonInfoComponent },
  { path: 'films', component: FilmsComponent },
  { path: 'films/info/:id', component: FilmInfoComponent },
  { path: 'vehicles', component: VehiclesComponent },
  { path: 'vehicles/info/:id', component: VehicleInfoComponent },
  { path: 'planets', component: PlanetsComponent },
  { path: 'planets/info/:id', component: PlanetInfoComponent },
  { path: 'starships', component: StarshipsComponent },
  { path: 'starships/info/:id', component: StarshipInfoComponent },
  { path: 'species', component: SpeciesComponent },
  { path: 'species/info/:id', component: SpeciesInfoComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
