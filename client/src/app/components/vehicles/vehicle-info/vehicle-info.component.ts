import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Vehicle } from 'src/app/models/vehicle.model';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-vehicle-info',
  templateUrl: './vehicle-info.component.html',
  styleUrls: ['./vehicle-info.component.css']
})
export class VehicleInfoComponent {
  public vehicle: Vehicle = {} as Vehicle;

  constructor (
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    let split = this.router.url.split('/');
    const routerId = Number.parseInt(split[split.length - 1]);

    const vhcl = this.dataService.getItem() as Vehicle;
    if(vhcl == null) {
      this.loadDataFromRouter(routerId);
    } else {
      split = vhcl.url.split('/');
      const vehicleId = Number.parseInt(split[split.length - 2]);

      if(routerId !== vehicleId) {
        this.loadDataFromRouter(routerId);
      } else {
        this.vehicle = vhcl;
      }
    } 
  }

  private loadDataFromRouter(id: number) {
    const obs = this.dataService.getVehicleById(id);
      obs.subscribe((vehicle: Vehicle) => {
        this.vehicle = vehicle;
    })
  }

  public getResourceFromURL(url: string) {
    let name: string = url;
    this.searchService.searchResourceByUrl(url).subscribe(result => {
      name = result[0].name;
    });
    return name;
  }

  public getRouterLink(url: string) {
    const split = url.split('/');
    return split[split.length - 2];
  }
}
