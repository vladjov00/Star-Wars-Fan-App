import { Component, Input } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { Vehicle, VehiclesPagination } from 'src/app/models/vehicle.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-vehicles',
  templateUrl: './vehicles.component.html',
  styleUrls: ['./vehicles.component.css']
})
export class VehiclesComponent {
  @Input()
  public vehicles: Observable<Vehicle[]> = {} as Observable<Vehicle[]>;

  public user: User | null = null;
  public pagination: Observable<VehiclesPagination>;

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.authService.user.subscribe((user: User | null) => this.user = user);
    this.authService.sendUserDataIfExists();
    if(!this.user) {
      console.log("Not logged in. Switching to login page");
      router.navigateByUrl('/login');
    }

    this.pagination = this.activatedRoute.queryParamMap.pipe(
      switchMap((queryMap: ParamMap) => {
        const page = queryMap.get("page");
        const pageNum: number = page != null ? Number.parseInt(page) : 1;
        return this.dataService.getVehiclesByPage(pageNum);
      })
    );
  }
}
