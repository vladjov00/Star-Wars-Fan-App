import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Vehicle } from 'src/app/models/vehicle.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-vehicle-item',
  templateUrl: './vehicle-item.component.html',
  styleUrls: ['./vehicle-item.component.css']
})
export class VehicleItemComponent {
  @Input() vehicle: Vehicle = {} as Vehicle;

  public vehicleId: number;

  constructor (
    private router: Router,
    private dataService: DataService
  ) {
    const split = this.router.url.split('/');
    this.vehicleId = Number.parseInt(split[split.length - 1]);
  }

  public getVehicleId(vehicle: Vehicle): string {
    const split = vehicle.url.split('/');
    return split[5];
  }

  public getVehicleInfo(vehicle: Vehicle): void {
    this.dataService.setItem(vehicle);
  }
}
