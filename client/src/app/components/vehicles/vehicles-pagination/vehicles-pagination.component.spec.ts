import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VehiclesPaginationComponent } from './vehicles-pagination.component';

describe('VehiclesPaginationComponent', () => {
  let component: VehiclesPaginationComponent;
  let fixture: ComponentFixture<VehiclesPaginationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VehiclesPaginationComponent]
    });
    fixture = TestBed.createComponent(VehiclesPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
