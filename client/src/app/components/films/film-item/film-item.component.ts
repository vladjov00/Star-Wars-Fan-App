import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Film } from 'src/app/models/film.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-film-item',
  templateUrl: './film-item.component.html',
  styleUrls: ['./film-item.component.css']
})
export class FilmItemComponent {
  @Input() 
  film: Film = {} as Film;

  public filmId: number;

  constructor (
    private router: Router,
    private dataService: DataService
  ) {
    const split = this.router.url.split('/');
    this.filmId = Number.parseInt(split[split.length - 1]);
  }

  public getFilmId(id: string): string {
    const split = id.split('/');
    return split[split.length - 2];
  }

  public getFilmInfo(film: Film): void {
    this.dataService.setItem(film);
  }
}
