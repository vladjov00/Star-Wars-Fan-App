import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Film } from 'src/app/models/film.model';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent {
  @Input()
  public films: Observable<Film[]> = {} as Observable<Film[]>;

  public user: User | null = null;

  constructor(
    private dataService: DataService,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.authService.user.subscribe((user: User | null) => this.user = user);
    this.authService.sendUserDataIfExists();
    if(!this.user) {
      console.log("Not logged in. Switching to login page");
      this.router.navigateByUrl('/login');
    }

    this.films = this.dataService.getAllFilmsNames();
  }
}
