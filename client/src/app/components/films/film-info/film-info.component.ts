import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Film } from 'src/app/models/film.model';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-film-info',
  templateUrl: './film-info.component.html',
  styleUrls: ['./film-info.component.css']
})
export class FilmInfoComponent {
  public film: Film = {} as Film;

  constructor (
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    let split = this.router.url.split('/');
    const routerId = Number.parseInt(split[split.length - 1]);

    const flm = this.dataService.getItem() as Film;
    if(flm == null) {
      this.loadDataFromRouter(routerId);
    } else {
      split = flm.url.split('/');
      const filmId = Number.parseInt(split[split.length - 2]);

      if(routerId !== filmId) {
        this.loadDataFromRouter(routerId);
      } else {
        this.film = flm;
      }
    } 
  }

  private loadDataFromRouter(id: number) {
    const obs = this.dataService.getFilmById(id);
      obs.subscribe((film: Film) => {
        this.film = film;
    })
  }

  public getResourceFromURL(url: string) {
    let name: string = url;
    this.searchService.searchResourceByUrl(url).subscribe(result => {
      name = result[0].name;
    });
    return name;
  }

  public getRouterLink(url: string) {
    const split = url.split('/');
    return split[split.length - 2];
  }
}
