import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Planet } from 'src/app/models/planet.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-planet-item',
  templateUrl: './planet-item.component.html',
  styleUrls: ['./planet-item.component.css']
})
export class PlanetItemComponent {
  @Input() planet: Planet = {} as Planet;

  public planetId: number;

  constructor (
    private router: Router,
    private dataService: DataService
  ) {
    const split = this.router.url.split('/');
    this.planetId = Number.parseInt(split[split.length - 1]);
  }

  public getPlanetId(planet: Planet): string {
    const split = planet.url.split('/');
    return split[5];
  }

  public getPlanetInfo(planet: Planet): void {
    this.dataService.setItem(planet);
  }
}
