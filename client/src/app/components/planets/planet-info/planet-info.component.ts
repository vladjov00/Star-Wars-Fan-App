import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Planet } from 'src/app/models/planet.model';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-planet-info',
  templateUrl: './planet-info.component.html',
  styleUrls: ['./planet-info.component.css']
})
export class PlanetInfoComponent {
  public planet: Planet = {} as Planet;

  constructor (
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    let split = this.router.url.split('/');
    const routerId = Number.parseInt(split[split.length - 1]);

    const plnt = this.dataService.getItem() as Planet;
    if(plnt == null) {
      this.loadDataFromRouter(routerId);
    } else {
      split = plnt.url.split('/');
      const planetId = Number.parseInt(split[split.length - 2]);

      if(routerId !== planetId) {
        this.loadDataFromRouter(routerId);
      } else {
        this.planet = plnt;
      }
    } 
  }

  private loadDataFromRouter(id: number) {
    const obs = this.dataService.getPlanetById(id);
      obs.subscribe((planet: Planet) => {
        this.planet = planet;
    })
  }

  public getResourceFromURL(url: string) {
    let name: string = url;
    this.searchService.searchResourceByUrl(url).subscribe(result => {
      name = result[0].name;
    });
    return name;
  }

  public getRouterLink(url: string) {
    const split = url.split('/');
    return split[split.length - 2];
  }
}
