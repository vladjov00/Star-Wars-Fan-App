import { Component, Input } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { Planet, PlanetsPagination } from 'src/app/models/planet.model';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-planets',
  templateUrl: './planets.component.html',
  styleUrls: ['./planets.component.css']
})
export class PlanetsComponent {
  @Input()
  public planets: Observable<Planet[]> = {} as Observable<Planet[]>;

  public user: User | null = null;
  public pagination: Observable<PlanetsPagination>;

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.authService.user.subscribe((user: User | null) => this.user = user);
    this.authService.sendUserDataIfExists();
    if(!this.user) {
      console.log("Not logged in. Switching to login page");
      this.router.navigateByUrl('/login');
    }

    this.pagination = this.activatedRoute.queryParamMap.pipe(
      switchMap((queryMap: ParamMap) => {
        const page = queryMap.get("page");
        const pageNum: number = page != null ? Number.parseInt(page) : 1;
        return this.dataService.getPlanetsByPage(pageNum);
      })
    );
  }
}
