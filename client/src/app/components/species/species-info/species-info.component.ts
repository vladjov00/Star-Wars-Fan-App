import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Species } from 'src/app/models/species.model';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-species-info',
  templateUrl: './species-info.component.html',
  styleUrls: ['./species-info.component.css']
})
export class SpeciesInfoComponent {
  public species: Species = {} as Species;

  constructor (
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    let split = this.router.url.split('/');
    const routerId = Number.parseInt(split[split.length - 1]);

    const spcs = dataService.getItem() as Species;
    if(spcs == null) {
      this.loadDataFromRouter(routerId);
    } else {
      split = spcs.url.split('/');
      const speciesId = Number.parseInt(split[split.length - 2]);

      if(routerId !== speciesId) {
        this.loadDataFromRouter(routerId);
      } else {
        this.species = spcs;
      }
    } 
  }

  private loadDataFromRouter(id: number) {
    const obs = this.dataService.getSpeciesById(id);
      obs.subscribe((species: Species) => {
        this.species = species;
    })
  }

  public getResourceFromURL(url: string) : string{
    let name: string = url;
    this.searchService.searchResourceByUrl(url).subscribe(result => {
      if(result[0] == undefined) {
        return;
      }

      name = result[0].name;
    });
    return name;
  }

  public getRouterLink(url: string): string {
    if(url == undefined) {
      return "";
    }
    const split = url.split('/');
    return split[split.length - 2];
  }
}
