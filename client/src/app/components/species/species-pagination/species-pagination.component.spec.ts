import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpeciesPaginationComponent } from './species-pagination.component';

describe('SpeciesPaginationComponent', () => {
  let component: SpeciesPaginationComponent;
  let fixture: ComponentFixture<SpeciesPaginationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SpeciesPaginationComponent]
    });
    fixture = TestBed.createComponent(SpeciesPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
