import { Component, Input } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, switchMap } from 'rxjs';
import { Species, SpeciesPagination } from 'src/app/models/species.model';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent {
  @Input()
  public species: Observable<Species[]> = {} as Observable<Species[]>;

  public user: User | null = null;
  public pagination: Observable<SpeciesPagination>;

  constructor(
    private dataService: DataService,
    private activatedRoute: ActivatedRoute,
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.authService.user.subscribe((user: User | null) => this.user = user);
    this.authService.sendUserDataIfExists();
    if(!this.user) {
      console.log("Not logged in. Switching to login page");
      this.router.navigateByUrl('/login');
    }
    
    this.pagination = this.activatedRoute.queryParamMap.pipe(
      switchMap((queryMap: ParamMap) => {
        const page = queryMap.get("page");
        const pageNum: number = page != null ? Number.parseInt(page) : 1;
        return this.dataService.getSpeciesByPage(pageNum);
      })
    );
  }
}
