import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Species } from 'src/app/models/species.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-species-item',
  templateUrl: './species-item.component.html',
  styleUrls: ['./species-item.component.css']
})
export class SpeciesItemComponent {
  @Input() species: Species = {} as Species;

  public speciesId: number;

  constructor (
    private router: Router,
    private dataService: DataService
  ) {
    const split = this.router.url.split('/');
    this.speciesId = Number.parseInt(split[split.length - 1]);
  }

  public getSpeciesId(species: Species): string {
    const split = species.url.split('/');
    return split[5];
  }

  public getSpeciesInfo(species: Species): void {
    this.dataService.setItem(species);
  }
}
