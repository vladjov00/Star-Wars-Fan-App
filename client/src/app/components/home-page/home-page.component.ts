import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Film } from 'src/app/models/film.model';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent {

  public user: User | null = null;

  constructor(
    private authService: AuthenticationService,
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    this.authService.user.subscribe((user: User | null) => this.user = user);
    this.authService.sendUserDataIfExists();

    if(!this.user) {
      console.log("Not logged in. Switching to login page");
      this.router.navigateByUrl('/login');
    }

    this.dataService.getAllPeopleNames().subscribe((people: { name: string, url: string }[]) => {
      this.searchService.setPeopleList(people);
    });
    this.dataService.getAllFilmsNames().subscribe((films: Film[]) => {
      this.searchService.setFilmsList(films);
    });
    this.dataService.getAllPlanetsNames().subscribe((planets: { name: string, url: string }[]) => {
      this.searchService.setPlanetsList(planets);
    });
    this.dataService.getAllSpeciesNames().subscribe((species: { name: string, url: string }[]) => {
      this.searchService.setSpeciesList(species);
    });
    this.dataService.getAllStarshipsNames().subscribe((starships: { name: string, url: string }[]) => {
      this.searchService.setStarshipsList(starships);
    });
    this.dataService.getAllVehiclesNames().subscribe((vehicles: { name: string, url: string }[]) => {
      this.searchService.setVehiclesList(vehicles);
    });
  }
}
