import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime, distinctUntilChanged, Observable, Subject, switchMap } from 'rxjs';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  @ViewChild('searchBox') searchBox: ElementRef<HTMLInputElement> | undefined;

  public searchForm: FormGroup;
  public searchResults!: Observable<{ name: string, id: string, type: string }[]>;
  private searchTerms = new Subject<string>();

  public peopleLoaded: boolean = true;
  public planetsLoaded: boolean = false;
  public speciesLoaded: boolean = false;
  public filmsLoaded: boolean = false;
  public starshipsLoaded: boolean = false;
  public vehiclesLoaded: boolean = false;
  public allLoaded: boolean = false;

  constructor(private searchService: SearchService) {
    this.searchForm = new FormGroup({
      search: new FormControl('',[])
    });

    this.peopleLoaded = this.searchService.peopleLoaded;
    this.starshipsLoaded = this.searchService.starshipsLoaded;
    this.speciesLoaded = this.searchService.speciesLoaded;
    this.filmsLoaded = this.searchService.filmsLoaded;
    this.vehiclesLoaded = this.searchService.vehiclesLoaded;
    this.planetsLoaded = this.searchService.planetsLoaded;
    this.allLoaded = this.peopleLoaded && this.speciesLoaded && this.planetsLoaded && this.starshipsLoaded && this.vehiclesLoaded && this.filmsLoaded;

    this.searchService.loadedDataSubject.subscribe(type => {
      switch(type) {
        case 'people': this.peopleLoaded = true; break;
        case 'species': this.speciesLoaded = true; break;
        case 'planets': this.planetsLoaded = true; break;
        case 'starships': this.starshipsLoaded = true; break;
        case 'vehicles': this.vehiclesLoaded = true; break;
        case 'films': this.filmsLoaded = true; break;
      };

      if(this.peopleLoaded && this.speciesLoaded && this.planetsLoaded && this.starshipsLoaded && this.vehiclesLoaded && this.filmsLoaded) {
        this.allLoaded = true;
      }
    });
  }

  ngOnInit(): void {
    this.searchResults = this.searchTerms.pipe(
      debounceTime(300),
      distinctUntilChanged(),
      switchMap((term: string) => this.searchService.search(term))
    )
  }

  public searchList(term: string): void {
    this.searchTerms.next(term);
  }

  public clearSearch() {
    this.searchBox!.nativeElement.value = "";
    this.searchList("");
  }
  
  public searchOnPress(): void {
    const data = this.searchForm.value;
    console.log(data);
  }

  public getPath(type: string): string {
    switch(type) {
      case 'Person': return 'people';
      case 'Species': return 'species';
      case 'Film': return 'films';
      case 'Vehicle': return 'vehicles';
      case 'Starship': return 'starships';
      case 'Planet': return 'planets';
      default: return '';
    }
  }

  private showSuggestions: boolean = false;

  public toggleSearchSuggestions(): void {
    this.showSuggestions = !this.showSuggestions;
  }

  public clickedOutside(): void {
    this.showSuggestions = false;
    this.searchList("");
  }
}
