import { Component, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnDestroy {
  loginForm: FormGroup;
  loginSub: Subscription = new Subscription();

  public errorMessage: string = "";

  constructor (
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.loginForm = new FormGroup({
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{4,}$/)]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
    });
  }

  public ngOnDestroy(): void {
    this.loginSub ? this.loginSub.unsubscribe() : null;
  }

  private invalidInputError(): string | null {
    let errors: ValidationErrors | null | undefined = this.loginForm.get('username')?.errors;

    if (errors != null) {
      if (errors['required']) {
        return 'Username must be provided.';
      }
      return 'Invalid username.';
    }

    errors = this.loginForm.get('password')?.errors;
    if (errors != null) {
      if (errors['required']) {
        return 'Password must be provided.';
      }
      return 'Invalid password.';
    }

    // Validation check passed
    return null;
  }


  public login(): void {
    this.errorMessage = '';
    if (this.loginForm.invalid) {
      this.errorMessage = this.invalidInputError()!;
      return;
    }

    const formData = this.loginForm.value;
    const obs: Observable<User | null | string> = this.authService.loginUser(formData.username, formData.password);

    this.loginSub = obs.subscribe((user: User | null | string) => {
      if(typeof(user) == "string") {
          this.errorMessage += user;
          return;
      }
      this.router.navigateByUrl("/home");
    });
  }
}
