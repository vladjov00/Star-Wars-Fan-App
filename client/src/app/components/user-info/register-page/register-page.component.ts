import { Component } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent {
  registerForm: FormGroup;
  registerSub: Subscription = new Subscription();

  public errorMessage: string = "";

  constructor (
    private authService: AuthenticationService,
    private router: Router
  ) {
    this.registerForm = new FormGroup({
      name: new FormControl("", [Validators.required]),
      username: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_-]{4,}$/)]),
      password: new FormControl("", [Validators.required, Validators.pattern(/^[0-9a-zA-Z_!@#$%^&*()+=`~-]{4,}$/)]),
    });
  }

  public ngOnDestroy(): void {
    this.registerSub ? this.registerSub.unsubscribe() : null;
  }

  private invalidInputError(): string | null {
    let errors: ValidationErrors | null | undefined = this.registerForm.get('username')?.errors;

    if (errors != null) {
      if (errors['required']) {
        return 'Username must be provided.';
      }
      return 'Username must contain at least 4 characters.';
    }

    errors = this.registerForm.get('password')?.errors;
    if (errors != null) {
      if (errors['required']) {
        return 'Password must be provided.';
      }
      return 'Password must contain at least 4 characters.';
    }

    errors = this.registerForm.get('name')?.errors;
    if (errors != null) {
      if (errors['required']) {
        return 'Name must be provided.';
      }
    }

    // Validation check passed
    return null;
  }

  public register(): void {
    this.errorMessage = '';
    if (this.registerForm.invalid) {
      this.errorMessage = this.invalidInputError()!;
      return;
    }

    const formData = this.registerForm.value;
    const obs: Observable<User | null> = this.authService.registerUser(formData.name, formData.username, formData.password);

    this.registerSub = obs.subscribe((user: User | null) => {
      console.log(user);
      this.router.navigateByUrl("/home");
    });
  }
}
