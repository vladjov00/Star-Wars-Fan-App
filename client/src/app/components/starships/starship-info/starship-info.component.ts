import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Starship } from 'src/app/models/starship.model';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-starship-info',
  templateUrl: './starship-info.component.html',
  styleUrls: ['./starship-info.component.css']
})
export class StarshipInfoComponent {
  public starship: Starship = {} as Starship;

  constructor (
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    let split = this.router.url.split('/');
    const routerId = Number.parseInt(split[split.length - 1]);

    const strshp = this.dataService.getItem() as Starship;
    if(strshp == null) {
      this.loadDataFromRouter(routerId);
    } else {
      split = strshp.url.split('/');
      const starshipId = Number.parseInt(split[split.length - 2]);

      if(routerId !== starshipId) {
        this.loadDataFromRouter(routerId);
      } else {
        this.starship = strshp;
      }
    } 
  }

  private loadDataFromRouter(id: number) {
    const obs = this.dataService.getStarshipById(id);
      obs.subscribe((starship: Starship) => {
        this.starship = starship;
    })
  }

  public getResourceFromURL(url: string) {
    let name: string = url;
    this.searchService.searchResourceByUrl(url).subscribe(result => {
      name = result[0].name;
    });
    return name;
  }

  public getRouterLink(url: string) {
    const split = url.split('/');
    return split[split.length - 2];
  }
}
