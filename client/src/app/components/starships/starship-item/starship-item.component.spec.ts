import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipItemComponent } from './starship-item.component';

describe('StarshipItemComponent', () => {
  let component: StarshipItemComponent;
  let fixture: ComponentFixture<StarshipItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StarshipItemComponent]
    });
    fixture = TestBed.createComponent(StarshipItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
