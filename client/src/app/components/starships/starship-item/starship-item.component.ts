import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Starship } from 'src/app/models/starship.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-starship-item',
  templateUrl: './starship-item.component.html',
  styleUrls: ['./starship-item.component.css']
})
export class StarshipItemComponent {
  @Input() starship: Starship = {} as Starship;

  public starshipId: number;

  constructor (
    private router: Router,
    private dataService: DataService
  ) {
    const split = this.router.url.split('/');
    this.starshipId = Number.parseInt(split[split.length - 1]);
  }

  public getStarshipId(starship: Starship): string {
    const split = starship.url.split('/');
    return split[5];
  }

  public getStarshipInfo(starship: Starship): void {
    this.dataService.setItem(starship);
  }
}
