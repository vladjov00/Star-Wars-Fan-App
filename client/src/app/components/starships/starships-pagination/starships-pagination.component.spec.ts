import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StarshipsPaginationComponent } from './starships-pagination.component';

describe('StarshipsPaginationComponent', () => {
  let component: StarshipsPaginationComponent;
  let fixture: ComponentFixture<StarshipsPaginationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [StarshipsPaginationComponent]
    });
    fixture = TestBed.createComponent(StarshipsPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
