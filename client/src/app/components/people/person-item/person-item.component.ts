import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from 'src/app/models/person.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-person-item',
  templateUrl: './person-item.component.html',
  styleUrls: ['./person-item.component.css']
})
export class PersonItemComponent {
  @Input() person: Person = {} as Person;

  public personId: number;

  constructor (
    private router: Router,
    private dataService: DataService
  ) {
    const split = this.router.url.split('/');
    this.personId = Number.parseInt(split[split.length - 1]);
  }

  public getPersonId(person: Person): string {
    const split = person.url.split('/');
    return split[5];
  }

  public getPersonInfo(person: Person): void {
    this.dataService.setItem(person);
  }
}
