import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from 'src/app/models/person.model';
import { DataService } from 'src/app/services/data.service';
import { SearchService } from 'src/app/services/search.service';

@Component({
  selector: 'app-person-info',
  templateUrl: './person-info.component.html',
  styleUrls: ['./person-info.component.css']
})
export class PersonInfoComponent {
  public person: Person = {} as Person;

  constructor (
    private dataService: DataService,
    private searchService: SearchService,
    private router: Router
  ) {
    let split = this.router.url.split('/');
    const routerId = Number.parseInt(split[split.length - 1]);

    const prsn = this.dataService.getItem() as Person;
    if(prsn == null) {
      this.loadDataFromRouter(routerId);
    } else {
      split = prsn.url.split('/');
      const personId = Number.parseInt(split[split.length - 2]);

      if(routerId !== personId) {
        this.loadDataFromRouter(routerId);
      } else {
        this.person = prsn;
      }
    } 
  }

  private loadDataFromRouter(id: number) {
    const obs = this.dataService.getPersonById(id);
      obs.subscribe((person: Person) => {
        this.person = person;
    })
  }

  public getResourceFromURL(url: string) : string{
    let name: string = url;
    this.searchService.searchResourceByUrl(url).subscribe(result => {
      if(result[0] == undefined) {
        return;
      }

      name = result[0].name;
    });
    return name;
  }

  public getRouterLink(url: string): string {
    if(url == undefined) {
      return "";
    }
    const split = url.split('/');
    return split[split.length - 2];
  }
}
