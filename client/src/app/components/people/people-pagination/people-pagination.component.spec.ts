import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PeoplePaginationComponent } from './people-pagination.component';

describe('PeoplePaginationComponent', () => {
  let component: PeoplePaginationComponent;
  let fixture: ComponentFixture<PeoplePaginationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PeoplePaginationComponent]
    });
    fixture = TestBed.createComponent(PeoplePaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
