import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PeoplePagination } from 'src/app/models/person.model';

@Component({
  selector: 'app-people-pagination',
  templateUrl: './people-pagination.component.html',
  styleUrls: ['./people-pagination.component.css']
})
export class PeoplePaginationComponent {

  @Input()
  pagination: PeoplePagination | undefined;

  private currentPage: number = 1;
  private maxPages: number = 10;

  constructor(private activatedRoute: ActivatedRoute) {
    this.currentPage = this.getCurrentPage();
  }

  public getArrayOfPageNumbers(count: number): number[] {
    const array = [...Array(Math.ceil(count/10)).keys()].map((page: number) => page + 1);
    this.maxPages = array[array.length - 1];
    return array;
  }

  public getCurrentPage(): number {
    this.activatedRoute.queryParams.subscribe((param) => {
      this.currentPage = param['page'];
    });
    return this.currentPage;
  }

  public getPreviousPage(): number {
    return Math.max(1, this.currentPage - 1);
  }

  public getNextPage(): number {
    return Math.min(this.maxPages, Number(this.currentPage) + Number(1));
  }
}
