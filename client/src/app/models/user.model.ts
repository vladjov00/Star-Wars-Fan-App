export class User {
    constructor(
      public id: string,
      public username: string,
      public name: string
    ) {}
}

export interface IJwtTokenData {
    id: string,
    username: string,
    name: string
}